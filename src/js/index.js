import $ from 'zepto-modules/zepto'
import 'zepto-modules/event'

import Rellax from 'rellax'


// Cache some DOM elements

const $window = $(window)
const $nav = $('#nav')
const $navToggle = $nav.find('.nav-toggle').first()
const $navMenu = $navToggle.next()


// Hamburger Menu!

$navToggle.on('click', () => $navMenu.toggleClass('is-active'))


// Parallax!

let rellax, rellaxRaw

const enableParallax = () => {
	if (!rellax) {
		rellax = new Rellax('.is-parallax', {
			speed: -1,
			center: false,
			round: true,
		})
	}

	if (!rellaxRaw) {
		rellaxRaw = new Rellax('.is-parallax--raw', {
			speed: -1,
			center: false,
			round: false,
		})
	}
}

const disableParallax = () => {
	if (rellax) rellax.destroy()
	if (rellaxRaw) rellaxRaw.destroy()
}


// Responsive things

const onRes = () => {
	const width = $window.width()

	if (width > 960) {
		enableParallax()
	} else {
		disableParallax()
	}
}

$window.resize(onRes)
onRes()


// Fixed Nav Shrinking!

let shrunk = false
let scrollPos = window.scrollY
const triggerPoint = 40

const shrinkNav = () => {
	if (!shrunk && scrollPos > triggerPoint) {
		$nav.addClass('nav--shrink')
		shrunk = true
	} else if (shrunk && scrollPos <= triggerPoint) {
		$nav.removeClass('nav--shrink')
		shrunk = false
	}
}

$(window).on('scroll', () => {
	scrollPos = window.scrollY
	shrinkNav()
})