Mandala Holdings
===============================================

Website development in progress

Add this to your hosts file:

    138.197.31.247 mandala.prod

Then add the remote to your repo:

	git remote add stage mandala@mandala.prod:stage

Developer Setup
---------------

	yarn    	#or npm install
	yarn start	#or npm run start
	#npm install -g sequelize-cli
	#sequelize db:migrate
