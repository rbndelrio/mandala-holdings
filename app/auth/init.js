const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy

const authenticationMiddleware = require('./middleware')

const bcrypt = require('bcryptjs')

const models = require('../models/index')
const User = models.User

passport.serializeUser(function (user, cb) {
	cb(null, user.username)
})

passport.deserializeUser(function (username, cb) {
	User.findOne({
		where: {
			'username': username
		}
	}).then((user) => {
		if (user == null) {
			cb(new Error('Wrong username'))
		}

		cb(null, user)
	})
})

const initPassport = () => {
	passport.use(new LocalStrategy(
		function(username, password, done) {
			User.findOne({
				where: {
					'username': username
				}
			}).then(function (user) {
				if (user == null) {
					return done(null, false, { message: 'Incorrect credentials.' })
				}

				var hashedPassword = bcrypt.hashSync(password, user.salt)

				if (user.password === hashedPassword) {
					return done(null, user)
				}

				return done(null, false, { message: 'Incorrect credentials.' })
			})
		}
	))
	passport.authenticationMiddleware = authenticationMiddleware
}

module.exports = initPassport