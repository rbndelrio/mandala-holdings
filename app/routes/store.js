const pages = [
	'about',
	'hotels',
	'restaurants',
	'portfolio',
	'philosophy',
	'contact',

	// 'admin',
	'login',
	'files',
	'register',
]

const nav = {
	main: [
		{
			slug: '',
			title: 'Home',
		},
		{
			slug: 'about',
			title: 'About',
		},
		{
			slug: 'portfolio',
			title: 'Portfolio',
		},
		{
			slug: 'philosophy',
			title: 'Philosophy',
		},
	],
	login: [
		{
			slug: 'login',
			title: 'Sign In',
		},
	],
	admin: [
		{
			slug: 'admin',
			title: 'Admin',
		},
		// {
		// 	slug: 'admin',
		// 	title: 'Content',
		// },
		// {
		// 	slug: 'admin',
		// 	title: 'Files',
		// },
	],
}

const about = {
	intro_section: {
		title: "About Us",
		description: "We achieve measurable success through a culture-focused, values-centered approach to business. Mandala Holdings understands how to work with nuanced and unique opportunities. Our versatility and creativity in all things we do are bound together with the common objective to succeed — no matter what the challenge or circumstance.",
		content: `
			<p>Headquartered in Miami, Mandala Holdings is quickly becoming one of the nation's premier investment platforms with a focus on developing hotels, restaurants and entertainment assets. Leveraging its South Florida base, Mandala Holdings is actively acquiring land for hotel development throughout the southeastern United States while expanding its activities in the food and beverage space.</p>
		`,
	},
	bio_section: {
		title: "Leadership Team",
		description: "Innovative and adaptable, our entrepreneurial team excels in all facets of the hospitality industry.",
		content: `
			<p>Our holistic approach to investment is the culmination of global best practices, elite experience, an insightful desire to succeed and a vision to continuously evolve. We understand change is the new reality in this dynamic world, and our goal is to always be evolving. We are well positioned to capitalize on today’s needs and tomorrow’s expectations at every point in industry life cycles.</p>
		`,
	},
	bios: [
		{
			image: "/img/vinay-rama.jpg",
			name: "Vinay Rama",
			position: "Founder &amp; Chief Executive Officer",
			description: `
				<p>Vinay is a lifelong hotelier with experience in hotel investments, development, operations and brokerage. After growing up in a family of hoteliers and getting a formal education in the industry, he spent a number of years with Hodges Ward Elliott, one of the most successful hotel brokerage/investment banking teams in the United States. Under Mark Elliott and his team, Vinay developed an in-depth understanding of the real estate valuation components of the hotel industry and the associated capital markets.</p>
				<p>At the age of 25, Vinay took his skills overseas to establish JHM International Hotels, in Mumbai, India. With his passion for international business, he helped grow the family’s investments significantly, including starting the first third-party management company in India and developing a university from the ground up on a 117-acre parcel. Intent on creating something very special based on everything he had learned, Vinay returned to the U.S. to found Mandala Holdings in 2013.</p>
				<p>Vinay has a hands-on approach to managing and decision-making, overseeing all activities at Mandala Holdings. He holds a B.S. degree from the Cornell University's School of Hotel Administration.</p>
			`,
		},
		{
			image: "/img/lee-babcock.jpg",
			name: "Lee Babcock",
			position: "Chief Investment Officer",
			description: `
				<p>Lee’s career in commercial real estate includes experience in office, retail and multifamily assets, with a focus on hospitality. He also worked in hospitality development for Gaylord Entertainment and the $1 billion private equity fund, RLJ Development, where he held the title of vice president. Lee joined Mandala Holdings as CIO in 2014.</p>
				<p>At Mandala Holdings, Lee focuses on ensuring acquisition and development projects unfold smoothly and expeditiously. He directs the activities of Mandala Holdings’ attorneys, architects, engineers, contractors and accountants to achieve outcomes that best serve the interests of the platform’s investors and partners. Lee holds a B.A. from the University of Virginia and an MBA from the University of Chicago Booth School of Business.</p>
			`,
		},
		{
			image: "/img/brittany-vaughn.jpg",
			name: "Brittany Vaughn",
			position: "Director of Asset Management",
			description: `
				<p>Brittany has extensive experience in the food service and retail industries, where she focused on operations and financial reporting. Prior to joining Mandala Holdings, Brittany worked at Ruby Tuesday, Inc. as an accountant, tax specialist and franchisee relationship manager.</p>
				<p>In her current role as director of asset management, Brittany draws on her financial analysis and accounting expertise to ensure that all of Mandala Holdings’ investments are evaluated and monitored consistently. Brittany’s value-add as an asset manager stems from her experience with the key factors that drive profitability in franchise operations. Brittany holds a BBA in accounting from East Tennessee State University.</p>
			`,
		},
	],
}

const portfolio = {
	intro_section: {
		title: "Portfolio",
		description: "Our portfolio can be described as one that generates memorable experiences. Through a well-thought-out vision and a passion to differentiate, we put personality and a timeless character into every asset we are a part of. Our guests and patrons feel the energy and ethos of that personality throughout every hotel, restaurant and entertainment experience.",
		content: `
			<p>At Mandala Holdings, we seek, discover and execute elusive and profitable investments. Our savvy is apparent in the fact that we create an environment in which uniquely positioned investment opportunities are either brought to the principals or are created through universal alignment. We attract prosperous opportunities based on our reliable reputation and unwavering dedication. We obtain highly sought-after projects once thought to be unattainable. The result? Landmark assets, one-of-a-kind outcomes and excellent returns. Synergistic encounters that are both exciting and unexpected are a common theme at Mandala Holdings.</p>
		`
	},
	portfolio_sections: [
		{
			title: "Hotels",
			items: [
				{
					image: "/upload/portfolio--2.jpg",
					alt: "Photo of property",
					title: "TownePlace Suites by Marriott",
					location: "Orlando, FL",
					content: `
						<a href='http://www.marriott.com/hotels/travel/mcoyw-towneplace-suites-orlando-at-seaworld/' target="_blank">Website</a>
						<ul>
							<li>188 rooms</li>
							<li>Premier location on International Drive</li>
							<li>Cutting-edge architecture and design attracts discerning vacationers and convention attendees</li>
							<li>Located in a hospitality sub-market with proven performance history</li>
							<li>Product custom-designed to meet and exceed target demographic demand expectations</li>
							<li>Opening May 2017</li>
						</ul>
					`,
				},
				{
					image: "/upload/portfolio--3.jpg",
					alt: "Photo of property",
					title: "Holiday Inn Express & Suites",
					location: "Orlando, FL",
					content: `
						<a href='https://www.ihg.com/holidayinnexpress/hotels/us/en/orlando/mcosw/hoteldetail' target="_blank">Website</a>
						<ul>
							<li>181 rooms</li>
							<li>Great location on International Drive, near convention center</li>
							<li>Pairs the power of a category-killer brand with a custom product that exceeds local demand expectations</li>
							<li>Incorporates unique features, including kids suites and a pool bar</li>
							<li>Opening May 2017</li>
						</ul>
					`,
				},
				{
					image: "/upload/portfolio--1.jpg",
					alt: "Photo of property",
					title: "Upper-Midscale Hotel",
					location: "Miami, FL",
					content: `
						<ul>
							<li>270 rooms plus Food & Beverage</li>
							<li>Highly desirable, strategic location with high barriers to entry</li>
							<li>Sourced off-market through extensive relationship building</li>
							<li>To be flagged with a top-tier, institutional brand</li>
							<li>Opening summer 2018</li>
						</ul>
					`,
				},
			],
		},
		{
			bg_title: "restaurants",
			title: "Restaurants &amp; Entertainment",
			items: [
				{
					image: "/upload/portfolio--4.jpg",
					alt: "Photo of property",
					title: "Lime Fresh Mexican Grill",
					content: `
						<a href="http://www.limefreshmexicangrill.com/" target="_blank">Website</a>
						<ul>
							<li>Health-conscious, fast-casual Mexican restaurant chain</li>
							<li>South Beach-born brand with locations throughout Florida</li>
							<li>Acquired the rights and portfolio from Ruby Tuesday in June 2016</li>
							<li>Looking forward to disciplined growth and a strengthening of Lime's loyal fan base</li>
						</ul>
					`,
				},
				{
					image: "/upload/portfolio--5.jpg",
					alt: "Photo of property",
					title: "Miami Bar &amp; Lounge",
					content: `
						<ul>
							<li>Upscale and intimate lounge in trendy Wynwood neighborhood</li>
							<li>Purposeful design with earthy, organic and craftsman features</li>
							<li>Lush greenery accentuated by natural light</li>
							<li>Stage for live music, comedy and entertainment</li>
							<li>Created by and for locals and travelers with elevated expectations</li>
						</ul>
					`,
				},
			]
		},
	],
	experience_section: {
		title: "Past Experience",
		description: "The principals at Mandala Holdings have played key roles in the following hospitality transactions:",
	},
	experience_items: [
		{
			title: "White Lodging Sale to RLJ",
			location: false,
			description: `
				<p>Member of the Elliott team at Hodges Ward Elliott that put together the White Lodging $1.7 billion/100-hotel sale to RLJ Development.</p>
			`,
		},
		{
			title: "Duet Hotels",
			location: "India",
			description: `
				<p>Directed GP role in a $250-million private equity fund focused on hotel investments. As GP, advised on the purchase of a portfolio of three hotels under various phases of development in the cities of Jaipur, Pune and Ahmadabad.</p>
			`,
		},
		{
			title: "Gaylord National",
			location: "Suburban Washington, DC",
			description: `
				<p>Development, financial analysis, government relations for a 2,000-room hotel with 400,000 sf conference center.</p>
			`,
		},
		{
			title: "Gaylord Chula Vista",
			location: "Suburban San Diego, CA",
			description: `
				<p>Directed all pre-development activities, including architectural studies, feasibility analysis, economic impact studies and extensive negotiations with local municipalities for a 1,500-room hotel with 450,000 sf conference center.</p>
			`,
		},
		{
			title: "Gaylord Texan",
			location: "Suburban Dallas, TX",
			description: `
				<p>Development and construction efforts for 1,411-room hotel with 450,000 sf conference center.</p>
			`,
		},
		{
			title: "Hilton Cancun",
			location: "Cancun, Mexico",
			description: `
				<p>Led development activities for expansion and master planning of a 468-room hotel with golf course and convention center.</p>
			`,
		},
		{
			title: "Aston Waikiki",
			location: "Honolulu, HI",
			description: `
				<p>Led acquisition and equity joint venture formation activities for a 450-room beachfront hotel and retail complex.</p>
			`,
		},
		{
			title: "Mohegan Sun Hotel",
			location: "Wilkes-Barre, PA",
			description: `
				<p>Assisted with pre-development feasibility studies, consultant management, government relations and equity joint venture formation for a 300-room headquarters hotel with 25,000 sf conference center adjacent to a racino gaming facility.</p>
			`,
		},
		{
			title: "Taj Gateway Hotel",
			location: "Surat, Gujarat, India",
			description: `
				<p>Renovation, repositioning and expansion of the 132-room Taj Gateway Hotel into a 206-room social hub, hotel and conference facility.</p>
			`,
		},
	],
}


const philosophy = {
	intro_section: {
		title: "Our Philosophy",
		description: "A mandala is more than a simple shape, just as Mandala Holdings is more than an investment firm. Our holistic approach to business is reflective of the symbol’s universal balance and perfection. Each of our experiences begins at a central point then expands into progress and possibilities."
	},
	blurbs: [
		{
			title: "In-depth asset management",
			content: `
				<p>Successful asset management is both an art and a science. Mandala Holdings capitalizes on these disciplines by combining detailed knowledge of each property with an analysis of the local market and competitive set. We add to this our deep and intuitive understanding of current clients and prospective customers at every operational level.</p>
				<p>This crafted and adaptive strategy has proven successful for Mandala Holdings time and time again. We draw upon our experience in owning and managing hotels to provide helpful, actionable and timely feedback to our management teams, whom we hold accountable on a systematic basis. We are owners, we are managers and we are developers. This well-rounded background enables us to asset manage intuitively, achieving excellence with an uncommon attention to detail.</p>
			`,
		},
		{
			title: "Comprehensive development management",
			content: `
				<p>The principals of Mandala Holdings have been actively involved in the entitlement, development and construction of significant commercial real estate projects in a number of countries. This experience, which stretches from hospitality and retail to office and industrial, guides our work as development managers. It also gives us greater ability to execute value engineering at every step. The ultimate goal: to ensure change orders are minimized and return on investment is maximized, allowing us to unleash a powerful yet cost-effective expression of the ownership’s vision.</p>
			`,
		},
	],
	footnote: {
		content: 'THIS WILL BE A SHORT RATIONALE THAT DESCRIBES THE MEANING BEHIND THE MANDALA LOGO. IM ASSUMING SOMETHING FROM OUR INITIAL PRESENTATION OF THE LOGOS.'
	}
}


module.exports = {
	pages,
	nav,
	about,
	portfolio,
	philosophy,
}