const express = require('express'),
      compression = require('compression')
      path = require('path'),
      favicon = require('serve-favicon'),
      logger = require('morgan'),
      cookieParser = require('cookie-parser'),
      bodyParser = require('body-parser'),

      passport = require('passport'),
      session = require('express-session'),
      // RedisStore = require('connect-redis')(session),

      content = require('./cms'),

      config = require('../config'),
      twigMarkdown = require('twig-markdown'),
      twig = require('twig'),

      app = express();


let routes // loaded asynchronously

// Memory diagnostics
// require('./mem');

// view engine setup
app.set('views', path.join(__dirname, 'views'))

twig.extend(twigMarkdown);

app.set('view engine', 'twig');
app.set("view options", { layout: false });

app.set("twig options", { 
    strict_variables: false
});

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')))
const shouldCompress = (req, res) => {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header 
    return false
  }
 
  // fallback to standard filter function 
  return compression.filter(req, res)
}

app.use(compression({filter: shouldCompress}))

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

require('./auth').init(app)

// app.use(session({
	//store: new RedisStore(config.redisStore.options),
// 	secret: config.redisStore.secret,
// 	resave: false,
// 	saveUninitialized: false
// }))
app.use(session({
	secret: config.redisStore.secret,
	resave: false,
	saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())


content.init().then(() => {
  routes = require('./routes/index')
  app.use('/', routes)

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    let err = new Error('Not Found')
    err.status = 404
    next(err)
  })

  // error handler
  app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render('error')
  })
})

module.exports = app
