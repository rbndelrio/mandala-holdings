'use strict';
module.exports = function(sequelize, DataTypes) {
  var File = sequelize.define('File', {
    filename: DataTypes.STRING,
    type: DataTypes.STRING,
    url: DataTypes.STRING,
    modified: DataTypes.DATE,
    bytesize: DataTypes.INTEGER,
    checksum: DataTypes.STRING,
    collection: DataTypes.ARRAY(DataTypes.STRING),
    users: DataTypes.ARRAY(DataTypes.STRING),
    etc: DataTypes.JSONB
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return File;
};