const pg = require('pg')

///////////////////<protocol>://<username>:<password>@<host>/<databaseName>
const defaultUrl = 'postgres://postgres_admin:mandela@localhost:5432/mandala'
const connectionString = process.env.DATABASE_URL || defaultUrl

const client = new pg.Client(connectionString);
client.connect(err => {
	if (err) throw err;
});
const query = client.query(
  'CREATE TABLE items(id SERIAL PRIMARY KEY, text VARCHAR(40) not null, complete BOOLEAN)');
query.on('end', () => { client.end(); });