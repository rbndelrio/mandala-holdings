'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Files', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      filename: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      url: {
        type: Sequelize.STRING,
        validate: {
          isUrl: true
        }
      },
      modified: {
        type: Sequelize.DATE
      },
      bytesize: {
        type: Sequelize.INTEGER
      },
      checksum: {
        type: Sequelize.STRING
      },
      users: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      collection: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      etc: {
        type: Sequelize.JSONB
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Files');
  }
};