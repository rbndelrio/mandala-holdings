const fs = require('fs');
const contentful = require('contentful');
const client = contentful.createClient({
  // This is the space ID. A space is like a project folder in Contentful terms
  space: 'ofxjeathja69',
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken: '16a5d6e6b37c6d646d9e01d300a4c8dcc7660f8ded61062d6a4cc751d4fa9ba9'
});

let path = 'data/content';
let store = {
	ready: false,
	data: {}
};

const writeFile = (data) => {
	let buffer = new Buffer(data);

	fs.open(path, 'w', function(err, fd) {
		if (err) {
			throw 'error opening file: ' + err;
		}

		fs.write(fd, buffer, 0, buffer.length, null, function(err) {
			if (err) throw 'error writing file: ' + err;
			fs.close(fd, function() {
				console.log('file written');
			})
		});
	});
}

const pageTypes = [
	'home',
	'about',
	'portfolio',
	'philosophy',
];

const getDataByType = type => client.getEntries({
	include: 5,
	content_type: type,
})

const fillContent = () => {
	const pages = []
	return new Promise((resolve, reject) => {
		const length = pageTypes.length

		pageTypes.forEach((pageType, i) => {
			pages[pages.length] = getDataByType(pageType)
				.then(data => { store[pageType] = data })
		})
		Promise.all(pages).then(vals => {
			if (store.onUpdate && store.ready) {
				store.onUpdate()
			}
			if (!store.ready) {
				setUpdate()
			}
			if (store.onReady && !store.ready) {
				store.ready = true;
				store.onReady()
			}
			resolve()
		})
	})
}
const minutes = 30
const interval = 1000*60*minutes // 45 minutes
let updating = false;

const update = force => {
	if (!updating || force) {
		console.log(`updating content... [${new Date()}]`)
		return fillContent()
	}
	return false
}

const setUpdate = () => {
	if (!updating) {
		updating = true
		console.log('setting automatic updates every ' + minutes +  ' minutes')
	}

	setTimeout(() => {
		update(true)
		setUpdate()
	}, interval)
}

const onInit = (cb) => {
	store.onReady = cb
}

const init = () => {
	return new Promise((resolve, reject) => {
		if (store.ready) {
			resolve()
		} else if (fs.existsSync(path)) {
			store.data = JSON.parse(fs.readFileSync(path, 'utf8'));
			console.log('reading from file')
			fillContent().then(resolve)
		} else {
			console.log('pulling from server')
			sync().then(fillContent).then(resolve)
		}
	})
}

const sync = () => client
	.sync({ initial: true })
	.then((data) => {
		const dataObj = JSON.parse(data.stringifySafe());
		const dataString = JSON.stringify(dataObj);

		store = dataObj;
		writeFile(dataString);

	}).catch((e) => {
		console.log(e);
	});

// const entryTypes = Object.keys(content);
// const entryData = content.getSpace();

// console.log(
// 	entryTypes
// );

// entryData.then((data) => {
// 	console.log(data);
// });

module.exports = {
	client,
	store,
	onInit,
	init,
	sync,
	writeFile,
	update,
};