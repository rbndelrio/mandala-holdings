// Modules
const gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),

	browsersync = require('browser-sync').create();

const path = {
	sass: {
		src: './app/sass/',
		bld: './app/public/css/',
	},
	twig: './app/views/',
	js: './app/views/',
}

// Sass Styles
gulp.task('sass', () => {
    return gulp.src([
            path.sass.src + 'style.scss'
        ])
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                './node_modules'
            ]
        }).on('error', sass.logError))
        .pipe(prefix({
            browsers: ['last 6 versions', 'ie 9', 'iOS 7', 'iOS 8', 'Safari >= 6'],
            cascade: false
        }))
        .pipe(gulp.dest(path.sass.bld))
        .pipe(browsersync.stream({match: '**/*.css'}))
})


gulp.task('watch', () => {
	browsersync.init({
		proxy: '127.0.0.1:8080',
        cors: true,
		port: 1234,
		open: false,
		notify: false,
		reloadOnRestart: true,
	});
	gulp.watch(path.sass.src + '**/*.scss',['sass']);
	//gulp.watch(path.twig + '**/*.twig').on("change", browsersync.reload);
	//gulp.watch(path.js + '**/*.js').on("change", browsersync.reload);
});

gulp.task('build', [
    'sass',
])