const redisStore = {
	options: {
	    // url: process.env.REDIS_STORE_URI || 'localhost:3000',
		host: 'localhost',
		port: 3000,
		ttl :  260
	},
    secret: process.env.REDIS_STORE_SECRET || 'shhhhhhh'
}

module.exports = {
	redisStore
}