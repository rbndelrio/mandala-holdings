const webpack = require('webpack');
const path = require('path');

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: {
    app: './src/js/index.js',
  },
  output: {
    filename: 'js.js',
    path: path.resolve('./app/public/js'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/, 
        exclude: '/node_modules/',
        loader: 'babel-loader',
        options: {
          presets: [
            ['es2015', { modules: false }],
          ],
        },
      }
    ],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
      }
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
  ],
}